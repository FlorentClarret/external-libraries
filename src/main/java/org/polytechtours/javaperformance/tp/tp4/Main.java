package org.polytechtours.javaperformance.tp.tp4;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import it.unimi.dsi.fastutil.longs.Long2IntAVLTreeMap;
import it.unimi.dsi.fastutil.longs.Long2IntSortedMap;
import javolution.util.FastTable;
import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

public final class Main {

    public static void main(final String[] args) {
        trove();
        guava();
        commons();
        javolution();
        fastutil();
    }

    public static void trove() {
        final TIntObjectMap<String> map = new TIntObjectHashMap<String>();

        for (int i = 0; i < 10; i++ ) {
            map.put(i, "i=" + i);
        }

        System.out.println(map);
    }

    public static void guava() {
        final String string = "0 1 2 3 4";
        final Splitter splitter = Splitter.onPattern(" ");

        for(final String str : splitter.split(string)) {
            System.out.println(str);
        }
    }

    public static void commons() {
        final List<String> list = Lists.newArrayList();

        CollectionUtils.addAll(list, Lists.<String>newArrayList("0", "1", "2"));

        System.out.println(list);
    }

    public static void javolution() {
        final FastTable<Integer> fastTable = new FastTable<Integer>();

        for (int i = 0; i < 10; i++) {
            fastTable.add(i, i);
        }

        System.out.println(fastTable);
    }

    public static void fastutil() {
        final Long2IntSortedMap map = new Long2IntAVLTreeMap();

        map.put(1, 5);
        map.put(2, 6);
        map.put(3, 7);

        System.out.println(map);
    }

}
